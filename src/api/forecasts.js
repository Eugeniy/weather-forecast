import axios from 'axios';
import { apiKey } from '../config/apyKey';

export function getForecasts(citiesId) {
  return axios.get(
    `http://api.openweathermap.org/data/2.5/group?id=${citiesId}&&units=metric&appid=${apiKey}`,
  );
}

export function getForecastsByCityName(name) {
  return axios.get(
    `http://api.openweathermap.org/data/2.5/weather?q=${name}&appid=${apiKey}`,
  );
}

// for hourly request not free for everyone
// export function getHourlyForecastsByCityName(name) {
//   console.log(name);
//   return axios.get(
//     `http://pro.openweathermap.org/data/2.5/forecast/hourly?q=${name}&appid=${apiKey}`,
//   );
// }
