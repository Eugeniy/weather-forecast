import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { getDetailCityForecast } from '../../store/modules/forecasts';
import './styles.css';

export function DetailForecast(props) {
  const name = props.match.params.name;

  const { detailForecast } = useSelector(
    (state) => state.forecastsReducer,
  );

  const dispatch = useDispatch();

  useEffect(() => {
    if (!detailForecast.id) {
      dispatch(getDetailCityForecast(name));
    }
  }, [detailForecast, name, dispatch]);

  if (!detailForecast.name) {
    return null;
  }

  const refreshData = () => {
    dispatch(getDetailCityForecast(name));
  };

  const { main, sys, weather, wind, dt } = detailForecast;
  const date = new Date(dt * 1000).toDateString();
  const sunrise = new Date(sys.sunrise * 1000).toLocaleString();
  const sunset = new Date(sys.sunset * 1000).toLocaleString();

  return (
    <div className="detailForecast">
      <h1>{`Detail ${name} forecast`}</h1>
      <div className="content">
        <div className="weatherDetails">
          <div className="data">
            Temperature:{' '}
            <span className="value">
              {Math.ceil(main.temp - 273.15)}°C
            </span>
          </div>
          <div className="data">
            Clouds:
            <span className="value">{weather[0].description}</span>
          </div>
          <div className="data">
            Date:<span className="value">{date}</span>
          </div>
          <div className="data">
            Wind:<span className="value">{wind.speed} m/s</span>
          </div>
          <div className="data">
            Pressure:<span className="value">{main.pressure}hPa</span>
          </div>
          <div className="data">
            Sunrise:<span className="value">{sunrise}</span>
          </div>
          <div className="data">
            Sundown:<span className="value">{sunset}</span>
          </div>
        </div>
        <div className="dayTemperature">
          <div className="hourly">Hourly day temperature</div>
        </div>
      </div>
      <button onClick={refreshData} className="refreshButton">
        Refresh
      </button>
    </div>
  );
}

export default DetailForecast;
