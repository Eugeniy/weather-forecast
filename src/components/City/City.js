import React from 'react';
import { useDispatch } from 'react-redux';
import { NavLink } from 'react-router-dom';
import { deleteCity } from '../../store/modules/forecasts';
import './styles.css';

const flagUrl = 'http://openweathermap.org/images/flags/';

function City(props) {
  const { name, sys, main, weather, wind, coord, id } = props.city;

  const dispatch = useDispatch();

  const removeCity = () => {
    dispatch(deleteCity(id));
  };

  return (
    <div className="cityContainer">
      <div className="cityInfo">
        <NavLink className="link" to={`detail-forecast/${name}`}>
          {name},{sys.country}{' '}
          <img
            alt=""
            src={`${flagUrl}${sys.country.toLowerCase()}.png`}
          />
          {weather[0].description}
          <p>
            {main.temp}°C, temperature from {main.temp_min} to{' '}
            {main.temp_max}
            °C, wind {wind.speed}m/s, {main.pressure}
            hpa, coords [{coord.lon}
            {','}
            {coord.lat}]
          </p>
        </NavLink>
        <button onClick={removeCity} className="deleteButton">
          +
        </button>
      </div>
    </div>
  );
}

export default City;
