import React, { useEffect, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import {
  getForecasts,
  getForecastsByCityName,
} from '../../store/modules/forecasts';
import City from '../City/City';
import './styles.css';

function StartScreen() {
  const dispatch = useDispatch();
  const [addCity, handleAddCity] = useState('');

  const { forecasts, error } = useSelector(
    (state) => state.forecastsReducer,
  );

  useEffect(() => {
    if (
      !forecasts.length &&
      localStorage.getItem('addedId') &&
      localStorage.getItem('addedId').length
    ) {
      dispatch(getForecasts());
    }
  }, [forecasts, dispatch]);

  const cityName = (evt) => {
    handleAddCity(evt.target.value);
  };

  const citySearch = () => {
    dispatch(getForecastsByCityName(addCity));
    document.getElementById('cityName').value = '';
  };

  return (
    <div className="cityWrapper">
      {forecasts.map((city) => {
        return (
          <div className="city" key={city.id}>
            <City city={city} />
          </div>
        );
      })}
      <div className="addCity">
        <input id="cityName" onChange={cityName} type="search" />{' '}
        <button type="submit" onClick={citySearch}>
          Add city
        </button>
      </div>
      {/*{error && <span className="error">{error}</span>}*/}
    </div>
  );
}

export default StartScreen;
