import { combineReducers } from 'redux';
import forecastsReducer from './forecasts';

const rootReducer = combineReducers({ forecastsReducer });
export default rootReducer;
