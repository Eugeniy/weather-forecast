export const GET_FORECASTS = 'GET_FORECASTS';
export const GET_FORECASTS_SUCCESS = 'GET_FORECASTS_SUCCESS';
export const GET_FORECASTS_FAIL = 'GET_FORECASTS_FAIL';

export const GET_FORECASTS_BY_CITY_NAME =
  'GET_FORECASTS_BY_CITY_NAME';
export const GET_FORECASTS_BY_CITY_NAME_SUCCESS =
  'GET_FORECASTS_BY_CITY_NAME_SUCCESS';
export const GET_FORECASTS_BY_CITY_NAME_FAIL =
  'GET_FORECASTS_BY_CITY_NAME_FAIL';

export const GET_DETAIL_CITY_FORECAST = 'GET_DETAIL_CITY_FORECAST';
export const GET_DETAIL_CITY_FORECAST_SUCCESS =
  'GET_DETAIL_CITY_FORECAST_SUCCESS';
export const GET_DETAIL_CITY_FORECAST_FAIL =
  'GET_DETAIL_CITY_FORECAST_FAIL';

// for hourly request not free for everyone
// export const GET_HOURLY_FORECAST = 'GET_HOURLY_FORECAST';
// export const GET_HOURLY_FORECAST_SUCCESS =
//   'GET_HOURLY_FORECAST_SUCCESS';
// export const GET_HOURLY_FORECAST_FAIL = 'GET_HOURLY_FORECAST_FAIL';

export const DELETE_CITY = 'DELETE_CITY';
export const DELETE_CITY_SUCCESS = 'DELETE_CITY_SUCCESS';
export const DELETE_CITY_FAIL = 'DELETE_CITY_FAIL';

const initialState = {
  forecasts: [],
  forecastsIds: localStorage.getItem('addedId')
    ? [JSON.parse(localStorage.getItem('addedId'))]
    : [],
  detailForecast: {},
  error: null,
  // hourlyForecast: {},
};

export default function forecastsReducer(
  state = initialState,
  action,
) {
  switch (action.type) {
    case GET_FORECASTS: {
      return {
        ...state,
      };
    }
    case GET_FORECASTS_SUCCESS: {
      return {
        ...state,
        forecasts: action.data,
      };
    }
    case GET_FORECASTS_FAIL: {
      return {
        ...state,
        error: action.error,
      };
    }
    case GET_FORECASTS_BY_CITY_NAME: {
      return {
        ...state,
      };
    }
    case GET_FORECASTS_BY_CITY_NAME_SUCCESS: {
      return {
        ...state,
        forecastByName: action.data,
      };
    }
    case GET_FORECASTS_BY_CITY_NAME_FAIL: {
      return {
        ...state,
        error: action.error,
      };
    }
    case GET_DETAIL_CITY_FORECAST: {
      return {
        ...state,
      };
    }
    case GET_DETAIL_CITY_FORECAST_SUCCESS: {
      return {
        ...state,
        detailForecast: action.data,
      };
    }
    case GET_DETAIL_CITY_FORECAST_FAIL: {
      return {
        ...state,
        error: action.error,
      };
    }
    // for hourly request not free for everyone
    // case GET_HOURLY_FORECAST: {
    //   return {
    //     ...state,
    //   };
    // }
    // case GET_HOURLY_FORECAST_SUCCESS: {
    //   return {
    //     ...state,
    //     hourlyForecast: action.data,
    //   };
    // }
    // case GET_HOURLY_FORECAST_FAIL: {
    //   return {
    //     ...state,
    //     error: action.error,
    //   };
    // }
    case DELETE_CITY: {
      return {
        ...state,
      };
    }
    case DELETE_CITY_SUCCESS: {
      return {
        ...state,
        city: action.data,
      };
    }
    case DELETE_CITY_FAIL: {
      return {
        ...state,
        error: action.error,
      };
    }
    default:
      return state;
  }
}

export function getForecasts(cities) {
  return {
    type: GET_FORECASTS,
    cities,
  };
}

export function getForecastsByCityName(name) {
  return {
    type: GET_FORECASTS_BY_CITY_NAME,
    name,
  };
}

export function getDetailCityForecast(name) {
  return {
    type: GET_DETAIL_CITY_FORECAST,
    name,
  };
}

// for hourly request not free for everyone
// export function getHourlyCityForecast(name) {
//   console.log(name);
//   return {
//     type: GET_HOURLY_FORECAST,
//     name,
//   };
// }

export function deleteCity(id) {
  return {
    type: DELETE_CITY,
    id,
  };
}
