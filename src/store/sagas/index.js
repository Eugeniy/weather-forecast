import { forecastsSaga } from './forecasts';
import { all, fork } from 'redux-saga/effects';

export default function* () {
  yield all([fork(forecastsSaga)]);
}
