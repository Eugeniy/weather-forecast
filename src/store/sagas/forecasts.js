import { takeLatest, call, put, select } from 'redux-saga/effects';
import {
  GET_FORECASTS,
  GET_FORECASTS_FAIL,
  GET_FORECASTS_SUCCESS,
  GET_FORECASTS_BY_CITY_NAME,
  GET_FORECASTS_BY_CITY_NAME_SUCCESS,
  GET_FORECASTS_BY_CITY_NAME_FAIL,
  DELETE_CITY,
  DELETE_CITY_SUCCESS,
  DELETE_CITY_FAIL,
  GET_DETAIL_CITY_FORECAST,
  GET_DETAIL_CITY_FORECAST_SUCCESS,
  GET_DETAIL_CITY_FORECAST_FAIL,
  // for hourly request not free for everyone
  // GET_HOURLY_FORECAST,
  // GET_HOURLY_FORECAST_SUCCESS,
  // GET_HOURLY_FORECAST_FAIL,
} from '../modules/forecasts';
import {
  getForecasts,
  getForecastsByCityName,
  // getHourlyForecastsByCityName,
} from '../../api/forecasts';

function* getForecastsSaga() {
  try {
    const citiesId = yield select(
      ({ forecastsReducer }) => forecastsReducer.forecastsIds,
    );
    const { data } = yield call(getForecasts, citiesId);
    yield put({
      type: GET_FORECASTS_SUCCESS,
      data: data.list,
    });
  } catch (error) {
    console.error(error);
    yield put({
      type: GET_FORECASTS_FAIL,
      error,
    });
  }
}

function* getForecastsByNameSaga(action) {
  try {
    const name = action.name;
    const data = yield call(getForecastsByCityName, name);
    const addedId = JSON.parse(localStorage.getItem('addedId')) || [];

    if (addedId.find((i) => i === data.data.id)) {
      yield put({
        type: GET_FORECASTS_BY_CITY_NAME_FAIL,
        error: 'Already added',
      });
    } else {
      const forecasts = yield select(
        ({ forecastsReducer }) => forecastsReducer.forecasts,
      );

      addedId.push(data.data.id);

      localStorage.setItem('addedId', JSON.stringify(addedId));

      const updatedForecasts = [...forecasts];
      updatedForecasts.push(data.data);

      yield put({
        type: GET_FORECASTS_BY_CITY_NAME_SUCCESS,
        data,
      });

      yield put({
        type: GET_FORECASTS_SUCCESS,
        data: updatedForecasts,
      });
    }
  } catch (error) {
    console.error(error);
    yield put({
      type: GET_FORECASTS_BY_CITY_NAME_FAIL,
      error: error.response.data.message,
    });
  }
}

function* getDetailForecastSaga(action) {
  try {
    const name = action.name;
    const data = yield call(getForecastsByCityName, name);
    yield put({
      type: GET_DETAIL_CITY_FORECAST_SUCCESS,
      data: data.data,
    });
  } catch (error) {
    console.error(error);
    yield put({
      type: GET_DETAIL_CITY_FORECAST_FAIL,
      error,
    });
  }
}

// for hourly request not free for everyone
// function* getHourlyForecastSaga(action) {
//   try {
//     const name = action.name;
//     const data = yield call(getHourlyForecastsByCityName, name);
//     console.log(data);
//     yield put({
//       type: GET_HOURLY_FORECAST_SUCCESS,
//       data: data.data,
//     });
//   } catch (error) {
//     console.error(error);
//     yield put({
//       type: GET_HOURLY_FORECAST_FAIL,
//       error,
//     });
//   }
// }

function* deleteCitySaga(action) {
  try {
    const id = action.id;

    const cityIds = JSON.parse(localStorage.getItem('addedId'));

    const updatedCityIds = cityIds.filter((item) => item !== id);

    localStorage.setItem('addedId', JSON.stringify(updatedCityIds));

    yield put({ type: DELETE_CITY_SUCCESS });
    const { data } = yield call(getForecasts, updatedCityIds);
    yield put({
      type: GET_FORECASTS_SUCCESS,
      data: data.list,
    });
  } catch (error) {
    console.error(error);
    yield put({ type: DELETE_CITY_FAIL, error });
  }
}

export function* forecastsSaga() {
  yield takeLatest(GET_FORECASTS, getForecastsSaga);
  yield takeLatest(
    GET_FORECASTS_BY_CITY_NAME,
    getForecastsByNameSaga,
  );
  yield takeLatest(GET_DETAIL_CITY_FORECAST, getDetailForecastSaga);
  // for hourly request not free for everyone
  // yield takeLatest(GET_HOURLY_FORECAST, getHourlyForecastSaga);
  yield takeLatest(DELETE_CITY, deleteCitySaga);
}
