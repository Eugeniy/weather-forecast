import React, { Component } from 'react';
import { Route, Redirect, Switch } from 'react-router-dom';
import StartScreen from '../../components/StartScreen/StartScreen';
import DetailForecast from '../../components/DetailForecast/DetailForecast';

export default class MainPage extends Component {
  render() {
    return (
      <Switch>
        <Route exact path="/main-page" component={StartScreen} />
        <Route
          exact
          path="/detail-forecast/:name?"
          component={DetailForecast}
        />
        <Redirect exact from="/" to="/main-page" />
      </Switch>
    );
  }
}
