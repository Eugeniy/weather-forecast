import React from 'react';
import { BrowserRouter as Router } from 'react-router-dom';
import { Route } from 'react-router-dom';
import MainPage from './pages/MainPage/MainPage';
import './App.css';

function App() {
  return (
    <div className="App">
      <Router>
        <Route path="/" component={MainPage} />
      </Router>
    </div>
  );
}

export default App;
